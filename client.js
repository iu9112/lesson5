const http = require('http');
const querystring = require('querystring');
const PORT = 3000;

let firstName = 'Alexander';
let lastName = 'Savenkov';
let hash, fullData = {};
let data = querystring.stringify({
	'lastName':lastName
});

let options = {                                                //Параметры запроса
	hostname: 'netology.tomilomark.ru',
	path: '/api/v1/hash',
	method: 'POST',
	headers: 
	{                                                        //Заголовки
		'Content-Type': 'application/x-www-form-urlencoded',
		'Firstname': firstName
	}
};
function handlerClient(response) //Обработчик ответа
{
	let data = '';
	response.on('data'
	, function (chunk) {
	data += chunk;
	});
	response.on('end'
	, function () 
	{
		fullData.firstName = firstName;
		fullData.lastName = lastName;
		hash = JSON.parse(data);
		fullData.hash = hash['hash'];
		console.log(fullData);//Вывод в консольм готового json
	});
}
let request = http.request(options,handlerClient);//Запрос
request.write(data);
request.end();